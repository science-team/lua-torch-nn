#!/usr/bin/make -f
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND  = #-Wall -pedantic
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

TMP=$(shell pwd)/debian/tmp
LUA51=/usr/bin/lua5.1
LUAJIT=/usr/bin/luajit
ROOTDIR=$(shell pwd)

TORCH_FLAGS_5_1= \
 -DLUA=/usr/bin/lua5.1 \
 -DLUA_INCDIR=/usr/include/lua5.1 \
 -DLUA_LIBDIR=/usr/lib \
 -DLUA_BINDIR=/usr/bin \
 -DLIBDIR=/usr/lib \
 -DLUALIB=lua5.1 \
 -DTorch_INSTALL_BIN_SUBDIR=bin/ \
 -DTorch_INSTALL_LIB_SUBDIR=lib/$(DEB_HOST_MULTIARCH)/ \
 -DTorch_INSTALL_INCLUDE_SUBDIR=include/ \
 -DCMAKE_VERBOSE_MAKEFILE=ON \
 -DCMAKE_BUILD_TYPE=Release \
 -DCMAKE_INSTALL_PREFIX=/usr \
 -DCMAKE_C_FLAGS="-I/usr/include/TH -I/usr/include/lua5.1/TH -g" \
 -DTorch_FOUND=ON \
 -DUSE_THNN_SO_VERSION=ON \
 -DTHNN_SO_VERSION="0"

%:
	dh $@ --buildsystem=lua --with lua

override_dh_auto_configure:
	ln -s . nn
	# generate THNN_h.lua
	set -e; mkdir build; cd build; cmake ..; cd ..; rm -rf build
	# configure libTHNN.so
	set -e; if ! test -d THNN.build; then mkdir THNN.build; fi
	set -e; cd THNN.build; cmake ../lib/THNN $(TORCH_FLAGS_5_1) \
		-DTorch_INSTALL_CMAKE_SUBDIR=/usr/share/libtorch-thnn-dev/
	# configure the rest lua stuff
	dh_auto_configure --buildsystem=lua
	# configure install template
	sed -e 's/#DEB_HOST_MULTIARCH#/$(DEB_HOST_MULTIARCH)/g' \
		< debian/libtorch-thnn.install.in > debian/libtorch-thnn.install
	sed -e 's/#DEB_HOST_MULTIARCH#/$(DEB_HOST_MULTIARCH)/g' \
		< debian/libtorch-thnn.links.in > debian/libtorch-thnn.links
	sed -e 's/#DEB_HOST_MULTIARCH#/$(DEB_HOST_MULTIARCH)/g' \
		< debian/libtorch-thnn-dev.install.in > debian/libtorch-thnn-dev.install

override_dh_auto_build:
	# build libTHNN.so
	dh_auto_build --buildsystem=cmake -B THNN.build/ --parallel
	# build the rest lua stuff
	dh_auto_build --buildsystem=lua

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	-$(RM) -rf deb.test
	# prepare lua files to launch a "nn.test()"
	set -e; if ! test -d deb.test/nn; then mkdir -p deb.test/nn; fi
	set -e; find . -name '*.so*' -exec cp -v '{}' deb.test/ \;
	set -e; find . -name '*.lua' -exec cp -v '{}' deb.test/nn/ \;
	set -e; cd deb.test; ln -s nn/init.lua nn.lua;
	-set -e; cd deb.test; \
		LD_LIBRARY_PATH=$(ROOTDIR)/deb.test \
		$(LUAJIT) -e "require 'nn'; nn.test();"
	dh_auto_test
endif

override_dh_auto_install:
	# install libTHNN.so
	dh_auto_install --buildsystem=cmake -B THNN.build
	# install the rest lua stuff
	dh_auto_install --buildsystem=lua

override_dh_auto_clean:
	-$(RM) nn THNN_h.lua
	-$(RM) -rf THNN.build 5.1-torch-nn .pc deb.test
	dh_auto_clean

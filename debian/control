Source: lua-torch-nn
Section: interpreters
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Mo Zhou <cdluminate@gmail.com>
Build-Depends: cmake,
               debhelper (>=11),
               dh-lua,
               libtorch-th-dev,
               lua-torch-torch7,
               luajit,
               lua-moses (>= 2.0.0~),
Standards-Version: 4.2.1
Homepage: https://github.com/torch/nn
Vcs-Git: https://salsa.debian.org/science-team/lua-torch-nn.git
Vcs-Browser: https://salsa.debian.org/science-team/lua-torch-nn

# Architecture Note:
# https://github.com/torch/torch7/issues/762#issuecomment-271497176
# supported: amd64 arm64 armel armhf ppc64el kfreebsd-amd64

Package: lua-torch-nn
Architecture: all
Depends: libtorch-thnn-dev (>= ${source:Version}),
         lua-torch-torch7,
         lua-moses (>= 2.0.0~),
         luajit | lua5.1,
         ${misc:Depends},
         ${shlibs:Depends}
Suggests: luajit
XB-Lua-Versions: ${lua:Versions}
Description: Neural Network Package for Torch Framework
 This package provides an easy and modular way to build and train
 simple or complex neural networks using Torch Framework:
 .
  * Modules are the bricks used to build neural networks.
    Each are themselves neural networks, but can be combined with
    other networks using containers to create complex neural networks:
 .
    + Module: abstract class inherited by all modules.
    + Containers: container classes.
    + Transfer functions: non-linear functions.
    + Simple layers: simple network layer like `Linear`.
    + Table layers: layers for manipulating `table`s.
    + Convolution layers: several kinds of convolutions.
 .
  * Criterions compute a gradient according to a given loss function
    given an input and a target:
 .
    + Criterions: a list of all criterions.
    + `MSECriterion`: the Mean Squared Error criterion used for regression;
    + `ClassNLLCriterion`: the Negative Log Likelihood criterion used for
      classification.
 .
  * Additional documentation:
 .
   + Overview of the package essentials including modules, containers
     and training.
   + Training: how to train a neural network using optim.
   + Testing: how to test your modules.
   + Experimental Modules: a package containing experimental modules and
     criteria.
 .
 This package is a core part of the Torch Framework.

Package: libtorch-thnn
Architecture: amd64 arm64 armel armhf ppc64el kfreebsd-amd64 i386
Section: libs
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: libTHNN.so of Neural Network Package for Torch Framework
 This package provides an easy and modular way to build and train
 simple or complex neural networks using Torch Framework.
 .
 This package contains libTHNN.so , backend library for lua-torch-nn.

Package: libtorch-thnn-dev
Architecture: amd64 arm64 armel armhf ppc64el kfreebsd-amd64 i386
Section: libdevel
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}, libtorch-thnn (= ${binary:Version})
Description: libTHNN.so of Neural Network Package for Torch Framework (dev)
 This package provides an easy and modular way to build and train
 simple or complex neural networks using Torch Framework.
 .
 This package contains headers for libTHNN.so .
